from environs import Env

env = Env()
env.read_env()

MONGODB_URL = env("MONGODB_URL")
